import { Component } from '@angular/core';

@Component({
    selector: "leftSideNav",
    templateUrl: "./leftSideNav.component.html"
})

export class LeftSideNavComponent { 

    clickedbutton: string = null;
    
    onClickPendingAgent(){
        this.clickedbutton = "Pending Agent";
    }

    onClickPendingClarification(){
        this.clickedbutton = "Pending Clarification";
    }

    onClickResolved(){
        this.clickedbutton = "Resolved";
    }

    onClickClosed(){
        this.clickedbutton = "Closed";
    }

    selectedbuttonvalue(): string {
        return this.clickedbutton;
    }
 }