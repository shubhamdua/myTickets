import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { LeftSideNavComponent } from './leftSideNav.component';
import { RightSideNavComponent } from './rightSideNav.component';
import { MiddleTicketSectionComponent } from './middleTicketSection.component';
//import { TicketPopupComponent } from './ticketPopup.component';

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ AppComponent, LeftSideNavComponent, RightSideNavComponent, MiddleTicketSectionComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
